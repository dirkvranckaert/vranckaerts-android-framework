package eu.vranckaert.framework.ui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

public class CustomDatePickerDialog extends DatePickerDialog {
        private CharSequence title;

        public CustomDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
            super(context, callBack, year, monthOfYear, dayOfMonth);
        }

        public final void setPermanentTitle(CharSequence title) {
            super.setTitle(title);
            this.title = title;
        }

        public final void setPermanentTitle(int titleResId) {
            super.setTitle(titleResId);
            this.title = this.getContext().getString(titleResId);
        }

        @Override
        public void onDateChanged(DatePicker view, int year, int month, int day) {
            super.onDateChanged(view, year, month, day);
            super.setTitle(title);
        }
    }