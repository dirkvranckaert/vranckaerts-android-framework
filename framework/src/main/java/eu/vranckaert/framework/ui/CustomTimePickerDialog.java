package eu.vranckaert.framework.ui;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class CustomTimePickerDialog extends TimePickerDialog {
    private CharSequence title;

    public CustomTimePickerDialog(Context context, OnTimeSetListener callBack, int hourOfDay, int minute,
                                  boolean is24HourView) {
        super(context, callBack, hourOfDay, minute, is24HourView);
    }

    public final void setPermanentTitle(CharSequence title) {
        super.setTitle(title);
        this.title = title;
    }

    public final void setPermanentTitle(int titleResId) {
        super.setTitle(titleResId);
        this.title = this.getContext().getString(titleResId);
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        super.onTimeChanged(view, hourOfDay, minute);
        super.setTitle(title);
    }
}