package eu.vranckaert.framework.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Date: 07/05/14
 * Time: 13:44
 *
 * @author Dirk Vranckaert
 */
public class NetworkHelper {
    private static final String LOG_TAG = NetworkHelper.class.getSimpleName();

    /**
     * Checks if the device is connected with internet or not.
     * @param ctx The app-context.
     * @return {@link Boolean#TRUE} if the device is connected or connecting, {@link Boolean#FALSE} if no connection is
     * available.
     */
    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            Log.d(LOG_TAG, "Device is online");
            return true;
        }
        Log.d(LOG_TAG, "Device is not online");
        return false;
    }
}
