package eu.vranckaert.framework.util;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Date: 31/03/14
 * Time: 15:51
 *
 * @author Dirk Vranckaert
 */
public class KeyboardHelper {
    public static void hideKeyboard(final MenuItem menuItem, final Context context) {
        View view = menuItem.getActionView();
        if (view != null) {
            hideKeyboard(view, context);
        }
    }

    public static void hideKeyboard(final View view, final Context context) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
