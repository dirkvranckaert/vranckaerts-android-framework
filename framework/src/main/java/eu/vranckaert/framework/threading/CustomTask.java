package eu.vranckaert.framework.threading;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.text.TextUtils;
import eu.vranckaert.framework.R;
import eu.vranckaert.framework.controller.AlertDialogFragment;
import eu.vranckaert.framework.controller.AlertDialogFragment.Builder;
import eu.vranckaert.framework.util.NetworkHelper;
import eu.vranckaert.framework.web.exception.CommunicationException;
import eu.vranckaert.framework.web.exception.EndpointUnreachableException;
import eu.vranckaert.framework.web.exception.WebException;

import java.util.Date;

/**
 * Date: 14/04/14
 * Time: 14:44
 *
 * @author Dirk Vranckaert
 */
public abstract class CustomTask {
    private Activity context;

    private Thread t;
    private boolean showLoadingDialog = false;
    private String loadingMessage;

    private int minTimeForBackgroundThread = 0;
    private boolean allowRetry;
    private boolean showErrorDialog;
    private boolean errorCancellable;
    private String errorTitle;
    private String errorMessage;
    private String retryButton;
    private String retryCancelButton;

    private AlertDialogFragment loadingFragment;
    private Exception exception;

    public CustomTask(Fragment fragment) {
        this(fragment.getActivity());
    }

    public CustomTask(Activity context) {
        this.context = context;
        setLoadingMessage(R.string.general_loading);
        setShowErrorDialog(true);
        setAllowRetry(true);
        setErrorCancellable(true);
        setRetryButton(R.string.general_retry_button);
        setRetryCancelButton(R.string.general_retry_cancel_button);
    }

    public Context getContext() {
        return context;
    }

    public final void execute(final CustomTaskCompletionListener listener, final Object... params) {
        exception = null;

        onPreExecute();

        Runnable r = new Runnable() {
            private long start;
            private long end;

            public void run() {
                try {
                    start = new Date().getTime();
                    final Object result = doInBackground(params);

                    end = new Date().getTime();
                    long duration = end - start;
                    if (minTimeForBackgroundThread > 0 && duration < minTimeForBackgroundThread) {
                        Thread.sleep(minTimeForBackgroundThread - duration);
                    }

                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissLoadingDialog();
                            if (listener != null) {
                                listener.onComplete(result);
                            }
                        }
                    });
                    return;
                } catch (WebException e) {
                    end = new Date().getTime();
                    setErrorTitle(R.string.general_error_web_title);
                    setErrorMessage(R.string.general_error_web_message);
                    exception = e;
                } catch (CommunicationException e) {
                    end = new Date().getTime();
                    setErrorTitle(R.string.general_error_web_title);
                    setErrorMessage(R.string.general_error_web_message);
                    exception = e;
                } catch (EndpointUnreachableException e) {
                    end = new Date().getTime();
                    if (NetworkHelper.isOnline(context)) {
                        setErrorTitle(R.string.general_error_server_offline_title);
                        setErrorMessage(R.string.general_error_server_offline_message);
                    } else {
                        setErrorTitle(R.string.general_error_offline_title);
                        setErrorMessage(R.string.general_error_offline_message);
                    }
                    exception = e;
                } catch (Exception e) {
                    end = new Date().getTime();
                    ErrorMapping errorMapping = getErrorMapping(e);
                    if (errorMapping == null) {
                        setErrorTitle(R.string.general_error_title);
                        setErrorMessage(R.string.general_error_message);
                    } else {
                        setErrorTitle(errorMapping.getTitle());
                        setErrorMessage(errorMapping.getMessage());
                    }
                    exception = e;
                }

                long duration = end - start;
                if (minTimeForBackgroundThread > 0 && duration < minTimeForBackgroundThread) {
                    try {
                        Thread.sleep(minTimeForBackgroundThread - duration);
                    } catch (InterruptedException e) {
                    }
                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissLoadingDialog();
                        onException(exception);
                        retry(listener, params);
                        exception = null;
                    }
                });
            }
        };
        t = new Thread(r);
        t.start();
    }

    public ErrorMapping getErrorMapping(Exception e) {
        return null;
    }

    private void dismissLoadingDialog() {
        if (loadingFragment != null) {
            loadingFragment.dismiss();
            loadingFragment = null;
        }
    }

    private void onPreExecute() {
        if (showLoadingDialog) {
            LoadingView loadingView = new LoadingView(context);
            loadingView.setMessage(loadingMessage);

            loadingFragment = AlertDialogFragment.getBuilder(context)
                    .setView(loadingView.getView())
                    .setCancelable(false)
                    .show();
        } else {
            loadingFragment = null;
        }

        preExecute();
    }

    public void preExecute() {
    }

    private void retry(final CustomTaskCompletionListener listener, final Object... params) {
        if (showErrorDialog) {
            Builder builder = AlertDialogFragment.getBuilder(context);
            builder.setMessage(errorMessage);
            if (errorCancellable) {
                builder.setNegativeButton(retryCancelButton, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (listener != null) {
                            listener.cancelOnError();
                        }
                        dialog.dismiss();
                    }
                });
            }
            builder.setCancelable(errorCancellable);
            builder.setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (listener != null) {
                        listener.cancelOnError();
                    }
                    dialog.dismiss();
                }
            });
            if (allowRetry) {
                builder.setPositiveButton(retryButton, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onRetry();
                        execute(listener, params);
                    }
                });
            }
            if (!TextUtils.isEmpty(errorTitle)) {
                builder.setTitle(errorTitle);
            }
            builder.show();
        }
    }

    public abstract Object doInBackground(Object... params) throws Exception;

    public void onRetry() {
    }

    public abstract void onException(Exception exception);

    public final void setLoadingMessage(int loadingMessageResId) {
        setLoadingMessage(context.getString(loadingMessageResId));
    }

    public final void setLoadingMessage(String loadingMessage) {
        showLoadingDialog = !TextUtils.isEmpty(loadingMessage);
        this.loadingMessage = loadingMessage;
    }

    public void setMinTimeForBackgroundThread(int minTimeForBackgroundThread) {
        this.minTimeForBackgroundThread = minTimeForBackgroundThread;
    }

    public final void setAllowRetry(boolean allowRetry) {
        this.allowRetry = allowRetry;
    }

    public final void setShowErrorDialog(boolean showErrorDialog) {
        this.showErrorDialog = showErrorDialog;
    }

    public void setErrorCancellable(boolean errorCancellable) {
        this.errorCancellable = errorCancellable;
    }

    public final void setErrorTitle(int errorTitleResId) {
        setErrorTitle(context.getString(errorTitleResId));
    }

    public final void setErrorTitle(String errorTitle) {
        this.errorTitle = errorTitle;
    }

    public final void setErrorMessage(int errorMessageResId) {
        setErrorMessage(context.getString(errorMessageResId));
    }

    public final void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public final void setRetryButton(int retryButtonResId) {
        setRetryButton(context.getString(retryButtonResId));
    }

    public final void setRetryButton(String retryButton) {
        this.retryButton = retryButton;
    }

    public final void setRetryCancelButton(int retryCancelButtonResId) {
        setRetryCancelButton(context.getString(retryCancelButtonResId));
    }

    public final void setRetryCancelButton(String retryCancelButton) {
        this.retryCancelButton = retryCancelButton;
    }

    public interface CustomTaskCompletionListener {
        void onComplete(Object result);

        void cancelOnError();
    }
}
