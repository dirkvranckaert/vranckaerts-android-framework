package eu.vranckaert.framework.threading;

/**
 * Date: 04/11/14
 * Time: 11:43
 *
 * @author Dirk Vranckaert
 */
public class ErrorMapping {
    private String title;
    private String message;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
