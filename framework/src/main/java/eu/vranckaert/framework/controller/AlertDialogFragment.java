package eu.vranckaert.framework.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;

/**
 * Date: 31/03/14
 * Time: 10:59
 *
 * @author Dirk Vranckaert
 */
public final class AlertDialogFragment extends DialogFragment implements AlertDialogListener {
    private static final String TAG = "DIALOG_FRAGMENT";

    private Builder builder;

    public AlertDialogFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        Dialog dialog = getDialog();
        if ((dialog != null) && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (builder.view != null) {
            detachViewFromParent(builder.view);
        }
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(builder.context);

        alertBuilder.setMessage(builder.message);
        if (!TextUtils.isEmpty(builder.positiveButton)) {
            alertBuilder.setPositiveButton(builder.positiveButton, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onPositiveButtonClicked();
                }
            });
        }
        if (!TextUtils.isEmpty(builder.negativeButton)) {
            alertBuilder.setNegativeButton(builder.negativeButton, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onNegativeButtonClicked();
                }
            });
        }
        if (builder.view != null) {
            alertBuilder.setView(builder.view);
        }

        AlertDialog dialog = alertBuilder.create();

        if (TextUtils.isEmpty(builder.title)) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        } else {
            dialog.setTitle(builder.title);
        }

        return dialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if (builder.onCancelListener != null) {
            builder.onCancelListener.onCancel(dialog);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (builder.onDismissListener != null) {
            builder.onDismissListener.onDismiss(dialog);
        }
    }

    private static void detachViewFromParent(View view) {
        ViewParent viewParent = view.getParent();
        if (viewParent != null && viewParent instanceof ViewGroup) {
            ((ViewGroup) viewParent).removeView(view);
        }
    }

    public static Builder getBuilder(Activity context) {
        Builder builder = new Builder();
        builder.context = context;
        return builder;
    }

    private void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public final static class Builder {
        private Activity context;
        private String message;
        private String title;
        private DialogInterface.OnClickListener positiveClickListener;
        private String positiveButton;
        private DialogInterface.OnClickListener negativeClickListener;
        private String negativeButton;
        private boolean cancelable = true;
        private View view;
        private DialogInterface.OnCancelListener onCancelListener;
        private DialogInterface.OnDismissListener onDismissListener;

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessage(int message) {
            return setMessage(context.getString(message));
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setTitle(int title) {
            return setTitle(context.getString(title));
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setPositiveButton(int button, DialogInterface.OnClickListener clickListener) {
            return setPositiveButton(context.getString(button), clickListener);
        }

        public Builder setPositiveButton(String postiveButton, DialogInterface.OnClickListener clickListener) {
            this.positiveButton = postiveButton;
            this.positiveClickListener = clickListener;
            return this;
        }

        public Builder setNegativeButton(int negativeButton, DialogInterface.OnClickListener clickListener) {
            return setNegativeButton(context.getString(negativeButton), clickListener);
        }

        public Builder setNegativeButton(String negativeButton, DialogInterface.OnClickListener clickListener) {
            this.negativeButton = negativeButton;
            this.negativeClickListener = clickListener;
            return this;
        }

        public Builder setView(View view) {
            this.view = view;
            return this;
        }

        public AlertDialogFragment show() {
            AlertDialogFragment fragment = new AlertDialogFragment();
            fragment.setCancelable(cancelable);
            fragment.setBuilder(this);
            FragmentManager manager = context.getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.add(fragment, AlertDialogFragment.TAG);
            trans.commitAllowingStateLoss();
            return fragment;
        }

        public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
            this.onCancelListener = onCancelListener;
            return this;
        }

        public Builder setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            this.onDismissListener = onDismissListener;
            return this;
        }
    }

    @Override
    public void onPositiveButtonClicked() {
        if (builder.positiveClickListener != null) {
            builder.positiveClickListener.onClick(getDialog(), AlertDialog.BUTTON_POSITIVE);
        } else {
            dismiss();
        }
    }

    @Override
    public void onNegativeButtonClicked() {
        if (builder.negativeClickListener != null) {
            builder.negativeClickListener.onClick(getDialog(), AlertDialog.BUTTON_NEGATIVE);
        } else {
            dismiss();
        }
    }
}
