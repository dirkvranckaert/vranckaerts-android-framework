package eu.vranckaert.framework.controller;

public interface AlertDialogListener {
    public void onPositiveButtonClicked();
    public void onNegativeButtonClicked();
}