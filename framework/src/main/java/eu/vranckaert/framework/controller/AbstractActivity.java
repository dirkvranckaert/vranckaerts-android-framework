package eu.vranckaert.framework.controller;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import eu.vranckaert.framework.R;
import eu.vranckaert.framework.controller.menu.MenuFragment;
import eu.vranckaert.framework.controller.menu.MenuFragment.MenuCallbacks;
import eu.vranckaert.framework.model.IMenuElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 29/03/14
 * Time: 09:13
 *
 * @author Dirk Vranckaert
 */
public abstract class AbstractActivity extends Activity implements MenuCallbacks {
    /**
     * The currently active content fragment
     */
    private AbstractFragment mContentFragment;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private MenuFragment mMenuFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private static final String SAVED_TITLE = "title";

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (enableSlidingMenu()) {
            setContentView(R.layout.activity_main);

            if (savedInstanceState == null) {
                int defaultSelectedMenuItem = getDefaultSelectedMenuItem();
                mMenuFragment = new MenuFragment(this, defaultSelectedMenuItem == -1 ? 0 : defaultSelectedMenuItem);
                getFragmentManager().beginTransaction()
                        .replace(R.id.menu_container, mMenuFragment, "menuFragment")
                        .commitAllowingStateLoss();

                // Set up the drawer.
                mMenuFragment.setUp(
                        R.id.menu_container,
                        (DrawerLayout) findViewById(R.id.drawer_layout));

                if (mTitle == null || mTitle.length() == 0) {
                    mTitle = getTitle();
                }
            } else {
                getFragmentManager().executePendingTransactions();
                mMenuFragment = (MenuFragment) getFragmentManager()
                        .findFragmentByTag("menuFragment");
                mMenuFragment.setUp(R.id.menu_container,
                        (DrawerLayout) findViewById(R.id.drawer_layout));
                mContentFragment = (AbstractFragment) getFragmentManager()
                        .findFragmentByTag(getClass().getSimpleName());

                mTitle = savedInstanceState.getCharSequence(SAVED_TITLE);
                setTitle(mTitle);
            }
        }

        doCreate(savedInstanceState);

        if (!enableSlidingMenu()) {
            if (savedInstanceState == null) {
                AbstractFragment fragment = createFragment();
                if (fragment == null) {
                    return;
                }
                if (getIntent() != null) {
                    fragment.setArguments(getIntent().getExtras());
                }
                putContentFragment(fragment);
            } else {
                getFragmentManager().executePendingTransactions();
                mContentFragment = (AbstractFragment) getFragmentManager()
                        .findFragmentByTag(getClass().getSimpleName());
            }
        }
    }

    protected int getDefaultSelectedMenuItem() {
        return 0;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(SAVED_TITLE, mTitle);
    }

    protected abstract void doCreate(Bundle savedInstanceState);

    protected AbstractFragment createFragment() {
        return null;
    }

    protected void putContentFragment(AbstractFragment fragment) {
        this.mContentFragment = fragment;
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public final void setTitle(String title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    public final void setTitle(int titleResId) {
        mTitle = getString(titleResId);
        getActionBar().setTitle(mTitle);
    }

    private void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public final boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        if (enableSlidingMenu()) {
            if (!mMenuFragment.isDrawerOpen()) {
                // Only show items in the action bar relevant to this screen
                // if the drawer is not showing. Otherwise, let the drawer
                // decide what to show in the action bar.
                doCreateContentOptionsMenu(menu);

                restoreActionBar();
                return true;
            }
        } else {
            doCreateContentOptionsMenu(menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    protected boolean enableSlidingMenu() {
        return true;
    }

    protected void doCreateContentOptionsMenu(Menu menu) {

    };

    public void doCreateSlidingOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    public boolean onMenuOptionsItemSelected(MenuItem item) {
        return false;
    }

    public List<IMenuElement> getMenuElements() {
        return new ArrayList<IMenuElement>();
    }

    public View getMenuView(LayoutInflater inflater, ViewGroup container) {
        return null;
    }

    public View getMenuElementView(IMenuElement menuElement, boolean selected, View convertView, Context context, ViewGroup parent) {
        return null;
    }

    public boolean onMenuItemSelected(IMenuElement menuElement) {
        return false;
    }

    public final void putFragmentFromMenu(AbstractFragment fragment) {
        putFragment(fragment, false);
    }

    public final void putFragment(AbstractFragment fragment, boolean addToBackStack) {
        this.mContentFragment = fragment;

        if (fragment !=  null) {
            if (getIntent() != null) {
                fragment.setArguments(getIntent().getExtras());
            }

            // update the main content by replacing fragments
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container, fragment, getClass().getSimpleName());
            if (addToBackStack) {
                ft.addToBackStack(null);
            }
            ft.commit();
        }
    }

    @Override
    public final void onBackPressed() {
        if (mContentFragment == null || !mContentFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }

    public final void superOnBackPressed() {
        super.onBackPressed();
    }

    public final MenuFragment getMenuFragment() {
        return mMenuFragment;
    }

    public final void setDisplayHomeAsUpEnabled(boolean showHomeAsUp) {
        getActionBar().setDisplayHomeAsUpEnabled(showHomeAsUp);
    }

    public final void setHomeButtonEnabled(boolean enabled) {
        getActionBar().setHomeButtonEnabled(enabled);
    }
}
