package eu.vranckaert.framework.controller.menu;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import eu.vranckaert.framework.R;
import eu.vranckaert.framework.controller.AbstractActivity;
import eu.vranckaert.framework.model.IMenuElement;

import java.util.List;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class MenuFragment extends Fragment {

    /**
     * This is the name of the preferences as stored locally in android especially for this framework's menu settings.
     */
    private static final String PREF_NAME = "framework_menu_preferences";
    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    /**
     * Remember if the navigation drawer is open or not.
     */
    private static final String STATE_DRAWER_OPEN = "navigation_drawer_is_open";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private MenuCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    /**
     * Allow a click on menu item that is active or not
     */
    private boolean mAllowClickOnActiveMenuItem = true;

    private AbstractActivity mCallbackActivity;
    private DrawerLayout mDrawerLayout;
    private View mMenu;
    private MenuAdapter mMenuAdapter;
    private ListView mMenuList;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = -1;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    private boolean mDrawerIsOpen;

    public MenuFragment() {

    }

    public MenuFragment(AbstractActivity callbackActivity, int defaultSelectedItem) {
        mCallbackActivity = callbackActivity;

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        mUserLearnedDrawer = getCallbackActivity().getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE).getBoolean(PREF_USER_LEARNED_DRAWER, false);

        mCurrentSelectedPosition = defaultSelectedItem;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mDrawerIsOpen = savedInstanceState.getBoolean(STATE_DRAWER_OPEN);
            mFromSavedInstanceState = true;
        }
    }

    private void setUserLearnedDrawer(boolean learned) {
        mUserLearnedDrawer = learned;
        Editor editor = getCallbackActivity().getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE).edit();
        editor.putBoolean(PREF_USER_LEARNED_DRAWER, learned);
        editor.commit();
    }

    public void resetUserLearnedPreferences() {
        setUserLearnedDrawer(false);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mMenu = getCallbackActivity().getMenuView(inflater, container);
        if (mMenu == null) {
            throw new IllegalStateException("When implementing a sliding menu the method AbstractActivity.getMenuView(LayoutInflater, ViewGroup) should be implemented!");
        }
        if (mMenu.findViewById(R.id.list) == null || !(mMenu.findViewById(R.id.list) instanceof ListView)) {
            throw new IllegalStateException("When implementing a sliding menu the menu view should contain a ListView with id R.id.list");
        }
        mMenuList = (ListView) mMenu.findViewById(R.id.list);
        mMenuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        mMenuAdapter = new MenuAdapter(
                getActionBar().getThemedContext(),
                getMenuElements(),
                mCallbackActivity
        );
        mMenuList.setAdapter(mMenuAdapter);
        handleSelectedItem(mCurrentSelectedPosition, null, mFromSavedInstanceState);

        return mMenu;
    }

    public List<IMenuElement> getMenuElements() {
        return getCallbackActivity().getMenuElements();
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    private class MenuAdapter extends BaseAdapter {
        private Context mContext;
        private List<IMenuElement> mMenuElements;
        private AbstractActivity mCallbackActivity;

        public MenuAdapter(Context context, List<IMenuElement> menuElements, AbstractActivity callbackActivity) {
            this.mContext = context;
            this.mMenuElements = menuElements;
            this.mCallbackActivity = callbackActivity;
        }

        @Override
        public int getCount() {
            return mMenuElements.size();
        }

        @Override
        public IMenuElement getItem(int position) {
            return mMenuElements.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            IMenuElement menuElement = getItem(position);

            return getCallbackActivity().getMenuElementView(menuElement, mCurrentSelectedPosition == position, convertView, mContext, parent);
        }
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getCallbackActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                mCallbackActivity,                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                mDrawerIsOpen = false;
                getCallbackActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    setUserLearnedDrawer(true);
                }

                mDrawerIsOpen = true;
                getCallbackActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
            setUserLearnedDrawer(true);
        } else if (mDrawerIsOpen && mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectItem(int position) {
        if (position == mCurrentSelectedPosition && !mAllowClickOnActiveMenuItem) {
            return;
        }

        IMenuElement menuElement = getMenuElements().get(position);
        if (!menuElement.isClickable()) {
            return;
        }

        if (position == mCurrentSelectedPosition && mDrawerLayout != null) {
            closeDrawer();
        }

        handleSelectedItem(position, menuElement, false);
    }

    private void handleSelectedItem(int position, IMenuElement menuElement, boolean fromSavedState) {
        if (menuElement == null) {
            menuElement = getMenuElements().get(position);
        }

        boolean callbackHandled = false;
        if (mCallbacks != null && !fromSavedState) {
            callbackHandled = mCallbacks.onMenuItemSelected(menuElement);
        }
        if ((callbackHandled || fromSavedState) && mDrawerLayout != null) {
            setSelectedMenuItem(menuElement, fromSavedState);
        } else {
            if (mMenuList != null) {
                mMenuList.setItemChecked(mCurrentSelectedPosition, true);
            }
        }

        if (mMenuAdapter != null) {
            mMenuAdapter.notifyDataSetChanged();
        }
    }

    public final void setSelectedMenuItem(IMenuElement menuElement) {
        this.setSelectedMenuItem(menuElement, false);

        if (mMenuAdapter != null) {
            mMenuAdapter.notifyDataSetChanged();
        }
    }

    private void setSelectedMenuItem(IMenuElement menuElement, boolean fromSavedState) {
        int position = getMenuElements().indexOf(menuElement);

        if (position != -1) {
            mCurrentSelectedPosition = position;
            if (mMenuList != null) {
                mMenuList.setItemChecked(position, true);
            }

            getCallbackActivity().setTitle(getString(menuElement.getScreenTitleResId()));

            if (!fromSavedState) {
                closeDrawer();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (MenuCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement MenuCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
        outState.putBoolean(STATE_DRAWER_OPEN, mDrawerIsOpen);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        // If the drawer is open, show the test_global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            getCallbackActivity().doCreateSlidingOptionsMenu(menu, inflater);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else {
            return getCallbackActivity().onMenuOptionsItemSelected(item);
        }
    }

    /**
     * Per the test_navigation drawer design guidelines, updates the action bar to show the test_global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return getCallbackActivity().getActionBar();
    }

    private AbstractActivity getCallbackActivity() {
        if (mCallbackActivity == null) {
            mCallbackActivity = (AbstractActivity) getActivity();
        }
        return mCallbackActivity;
    }

    public void setAllowClickOnActiveMenuItem(boolean allowClickOnActiveMenuItem) {
        this.mAllowClickOnActiveMenuItem = allowClickOnActiveMenuItem;
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface MenuCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        boolean onMenuItemSelected(IMenuElement menuElement);
    }
}
