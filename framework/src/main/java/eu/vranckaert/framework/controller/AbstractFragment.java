package eu.vranckaert.framework.controller;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Date: 29/03/14
 * Time: 09:14
 *
 * @author Dirk Vranckaert
 */
public abstract class AbstractFragment extends Fragment {
    public int RESULT_OK = Activity.RESULT_OK;
    public int RESULT_CANCELED = Activity.RESULT_CANCELED;
    public int RESULT_FIRST_USER = Activity.RESULT_FIRST_USER;

    public AbstractFragment() {

    }

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        setHasOptionsMenu(true);

        doCreate(savedInstanceState);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = doCreateView(inflater, container, savedInstanceState);

        return view;
    }

    protected void setResult(int resultCode) {
        getActivity().setResult(resultCode);
    }

    protected void setResult(int resultCode, Intent data) {
        getActivity().setResult(resultCode, data);
    }

    protected void finish() {
        getActivity().finish();
    }

    protected boolean onBackPressed() {
        return false;
    }

    protected abstract void doCreate(Bundle savedInstanceState);

    protected abstract View doCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
}
