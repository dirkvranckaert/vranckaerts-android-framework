package eu.vranckaert.framework.model;

/**
 * Date: 02/04/14
 * Time: 10:26
 *
 * @author Dirk Vranckaert
 */
public interface IMenuElement {
    int getMenuTitleResId();

    int getIconResId();

    int getScreenTitleResId();

    boolean isDivider();

    boolean isClickable();
}
