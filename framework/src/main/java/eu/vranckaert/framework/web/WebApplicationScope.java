package eu.vranckaert.framework.web;

import org.apache.http.client.HttpClient;

/**
 * Date: 14/04/14
 * Time: 11:55
 *
 * @author Dirk Vranckaert
 */
public interface WebApplicationScope {
    HttpClient getHttpClient();

    void setHttpClient(HttpClient client);
}
