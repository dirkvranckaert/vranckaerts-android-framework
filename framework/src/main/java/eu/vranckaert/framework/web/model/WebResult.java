package eu.vranckaert.framework.web.model;

import org.apache.http.Header;

import java.util.HashMap;
import java.util.Map;

/**
 * Date: 17/06/13
 * Time: 15:41
 *
 * @author Dirk Vranckaert
 */
public class WebResult {
    private int httpResponseCode;
    private JsonResult jsonResult;
    private Map<String, String> headers;

    public WebResult(int httpResponseCode, JsonResult jsonResult, Header[] headers) {
        this.httpResponseCode = httpResponseCode;
        this.jsonResult = jsonResult;
        this.headers = new HashMap<String, String>();
        for (Header header : headers) {
            this.headers.put(header.getName(), header.getValue());
        }
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public JsonResult getJsonResult() {
        return jsonResult;
    }

    public void setJsonResult(JsonResult jsonResult) {
        this.jsonResult = jsonResult;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
