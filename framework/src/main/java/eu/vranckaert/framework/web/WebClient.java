package eu.vranckaert.framework.web;

import android.text.TextUtils;
import android.util.Log;
import eu.vranckaert.framework.web.exception.AuthorizationRequiredException;
import eu.vranckaert.framework.web.exception.CommunicationException;
import eu.vranckaert.framework.web.exception.EndpointUnreachableException;
import eu.vranckaert.framework.web.exception.MethodNotAllowedException;
import eu.vranckaert.framework.web.exception.NotFoundException;
import eu.vranckaert.framework.web.exception.WebClientException;
import eu.vranckaert.framework.web.exception.WebException;
import eu.vranckaert.framework.web.exception.WebServerException;
import eu.vranckaert.framework.web.model.AuthorizationHeader;
import eu.vranckaert.framework.web.model.JsonEntity;
import eu.vranckaert.framework.web.model.JsonResult;
import eu.vranckaert.framework.web.model.WebResult;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Date: 14/04/14
 * Time: 11:59
 *
 * @author Dirk Vranckaert
 */
public class WebClient {
    private static final int ENDPOINT_TEST_TIMEOUT = 3000;

    private static WebClient instance = new WebClient();

    private WebApplicationScope applicationScope;
    private String baseUrl;
    private String methodName;
    private String testMethodName;
    private boolean autoTestEndpointAvailability = false;
    private AuthorizationHeader authorizationHeader;
    private Map<String, String> headerParams;
    private Map<String, Object> ampParams;
    private JsonEntity jsonEntity;
    private String[] parameters;

    private String encoding = "UTF-8";
    private String contentType = "application/json";

    private HttpPost httpPost;

    private WebClient() {
    }

    public static WebClient getInstance() {
        return instance;
    }

    public static void setInstance(WebClient instance) {
        WebClient.instance = instance;
    }

    private void clear() {
        this.baseUrl = null;
        this.methodName = null;
        this.authorizationHeader = null;
        this.ampParams = null;
        this.jsonEntity = null;
        this.parameters = null;
    }

    public WebClient setApplicationScope(WebApplicationScope applicationScope) {
        this.applicationScope = applicationScope;
        return this;
    }

    public WebClient setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public WebClient setMethodName(String methodName) {
        this.methodName = methodName;
        return this;
    }

    public WebClient setTestMethodName(String testMethodName) {
        this.testMethodName = testMethodName;
        return this;
    }

    public WebClient setAutoTestEndpointAvailability(boolean autoTestEndpointAvailability) {
        this.autoTestEndpointAvailability = autoTestEndpointAvailability;
        return this;
    }

    public WebClient setAuthorizationHeader(AuthorizationHeader authorizationHeader) {
        this.authorizationHeader = authorizationHeader;
        return this;
    }

    public WebClient setHeaderParams(Map<String, String> headerParams) {
        this.headerParams = headerParams;
        return this;
    }

    public WebClient setAmpParams(Map<String, Object> ampParams) {
        this.ampParams = ampParams;
        return this;
    }

    public WebClient setJsonEntity(JsonEntity jsonEntity) {
        this.jsonEntity = jsonEntity;
        return this;
    }

    public WebClient setParameters(String[] parameters) {
        this.parameters = parameters;
        return this;
    }

    public WebClient setEncoding(String encoding) {
        this.encoding = encoding;
        return this;
    }

    public WebClient setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    private HttpClient getClient() {
        HttpClient client = null;

        if (applicationScope != null) {
            client = applicationScope.getHttpClient();
        }

        if (client == null) {
            client = new DefaultHttpClient();
            if (applicationScope != null) {
                applicationScope.setHttpClient(client);
            }
        }

        return client;
    }

    public void testEndpointAvailability() throws EndpointUnreachableException {
        if (!TextUtils.isEmpty(baseUrl) && !TextUtils.isEmpty(testMethodName)) {
            String endpoint = baseUrl + testMethodName;
            if (!isEndpointAvailable(endpoint)) {
                throw new EndpointUnreachableException(endpoint);
            }
        }
    }

    public final WebResult get() throws WebException, CommunicationException, EndpointUnreachableException {
        checkMinimumParameters();

        if (autoTestEndpointAvailability) {
            testEndpointAvailability();
        }

        String endpoint = baseUrl + methodName;

        if (parameters != null && parameters.length > 0) {
            for (String param : parameters) {
                try {
                    param = URLEncoder.encode(param, encoding);
                } catch (UnsupportedEncodingException e) {
                }
                endpoint += "/" + param;
            }
        }

        endpoint = buildEndpointWithAmpParams(endpoint, ampParams);

        HttpGet httpGet = new HttpGet(endpoint);
        if (authorizationHeader != null) {
            httpGet.setHeader("Authorization", authorizationHeader.getContent());
        }

        if (headerParams != null) {
            for (Map.Entry<String, String> entry : headerParams.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                httpGet.setHeader(key, value);
            }
        }

        HttpClient client = getClient();
        try {
            HttpResponse response = client.execute(httpGet);
            return handleHttpResponse(response);
        } catch (ClientProtocolException e) {
            clear();
            throw new CommunicationException(e);
        } catch (IOException e) {
            clear();
            throw new CommunicationException(e);
        }
    }

    public final WebResult post() throws WebException, CommunicationException, EndpointUnreachableException {
        checkMinimumParameters();

        if (autoTestEndpointAvailability) {
            testEndpointAvailability();
        }

        String endpoint = baseUrl + methodName;

        if (parameters != null && parameters.length > 0) {
            for (Object param : parameters) {
                endpoint += "/" + param;
            }
        }

        endpoint = buildEndpointWithAmpParams(endpoint, ampParams);

        httpPost = new HttpPost(endpoint);
        httpPost.setHeader(new BasicHeader(HTTP.CONTENT_ENCODING, encoding));
        httpPost.setHeader(new BasicHeader(HTTP.CONTENT_TYPE, contentType));
        if (authorizationHeader != null) {
            httpPost.setHeader("Authorization", authorizationHeader.getContent());
        }

        if (headerParams != null) {
            for (Map.Entry<String, String> entry : headerParams.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                httpPost.setHeader(key, value);
            }
        }

        if (jsonEntity != null) {
            String data = jsonEntity.toJSON();

            try {
                StringEntity entity = new StringEntity(data, encoding);
                entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, contentType));
                httpPost.setEntity(entity);
            } catch (UnsupportedEncodingException e) {
            }
        }

        HttpClient client = getClient();
        try {
            HttpResponse response = client.execute(httpPost);
            return handleHttpResponse(response);
        } catch (UnknownHostException e) {
            clear();
            throw new CommunicationException(e);
        } catch (IOException e) {
            clear();
            throw new CommunicationException(e);
        }
    }

    public final WebResult put() throws WebException, CommunicationException, EndpointUnreachableException {
        checkMinimumParameters();

        if (autoTestEndpointAvailability) {
            testEndpointAvailability();
        }

        String endpoint = baseUrl + methodName;

        if (parameters != null && parameters.length > 0) {
            for (Object param : parameters) {
                endpoint += "/" + param;
            }
        }

        endpoint = buildEndpointWithAmpParams(endpoint, ampParams);

        HttpPut httpPut = new HttpPut(endpoint);
        if (authorizationHeader != null) {
            httpPut.setHeader("Authorization", authorizationHeader.getContent());
        }

        if (headerParams != null) {
            for (Map.Entry<String, String> entry : headerParams.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                httpPut.setHeader(key, value);
            }
        }

        HttpClient client = getClient();
        try {
            HttpResponse response = client.execute(httpPut);
            return handleHttpResponse(response);
        } catch (ClientProtocolException e) {
            clear();
            throw new CommunicationException(e);
        } catch (IOException e) {
            clear();
            throw new CommunicationException(e);
        }
    }

    public final int delete() throws CommunicationException, EndpointUnreachableException {
        checkMinimumParameters();

        if (autoTestEndpointAvailability) {
            testEndpointAvailability();
        }

        String endpoint = baseUrl + methodName;

        if (parameters != null && parameters.length > 0) {
            for (Object param : parameters) {
                endpoint += "/" + param;
            }
        }

        endpoint = buildEndpointWithAmpParams(endpoint, ampParams);

        HttpDeleteWithBody httpDelete = new HttpDeleteWithBody(endpoint);
        if (authorizationHeader != null) {
            httpDelete.setHeader("Authorization", authorizationHeader.getContent());
        }

        if (headerParams != null) {
            for (Map.Entry<String, String> entry : headerParams.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                httpDelete.setHeader(key, value);
            }
        }

        if (jsonEntity != null) {
            String data = jsonEntity.toJSON();

            try {
                StringEntity entity = new StringEntity(data, encoding);
                entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, contentType));
                httpDelete.setEntity(entity);
            } catch (UnsupportedEncodingException e) {
            }
        }

        HttpClient client = getClient();
        try {
            HttpResponse response = client.execute(httpDelete);
            if (response != null) {
                return response.getStatusLine().getStatusCode();
            } else {
                return HttpStatusCode.UNHANDLED_EXCEPTION;
            }
        } catch (ClientProtocolException e) {
            throw new CommunicationException(e);
        } catch (IOException e) {
            throw new CommunicationException(e);
        }
    }

    private void checkMinimumParameters() {
        if (TextUtils.isEmpty(baseUrl) && TextUtils.isEmpty(methodName)) {
            throw new IllegalArgumentException("Missing the baseUrl and methodName in the WebClient");
        } else if (TextUtils.isEmpty(baseUrl) && !TextUtils.isEmpty(methodName)) {
            throw new IllegalArgumentException("Missing the baseUrl in the WebClient");
        } else if (!TextUtils.isEmpty(baseUrl) && TextUtils.isEmpty(methodName)) {
            throw new IllegalArgumentException("Missing the methodName in the WebClient");
        }
    }

    public void clearCookies() {
        if (getClient() instanceof DefaultHttpClient) {
            ((DefaultHttpClient) getClient()).getCookieStore().clear();
        }
    }

    public void abort() {
        try {
            if (httpPost != null) {
                httpPost.abort();
                httpPost = null;
            }
        } catch (Exception e) {
        }
    }

    public boolean isEndpointAvailable(String endpoint) {
        return isEndpointAvailable(endpoint, ENDPOINT_TEST_TIMEOUT);
    }

    public boolean isEndpointAvailable(String endpoint, int timeout) {
        try {
            URL url = new URL(endpoint);
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setConnectTimeout(timeout);
            urlc.connect();
            if (urlc.getResponseCode() == HttpStatusCode.OK) {
                return true;
            }
        } catch (MalformedURLException e1) {
            Log.d(WebClient.class.getSimpleName(), "Mallformed URL Exception");
        } catch (IOException e) {
            Log.d(WebClient.class.getSimpleName(), "IO Exception");
        }
        return false;
    }

    private String buildEndpointWithAmpParams(String endpoint, Map<String, Object> ampParams) {
        if (ampParams == null || ampParams.size() == 0) {
            return endpoint;
        }

        endpoint += "?";

        for (Map.Entry<String, Object> entry : ampParams.entrySet()) {
            if (!endpoint.endsWith("?")) {
                endpoint += "&";
            }

            if (entry.getValue() instanceof String) {
                String key = "";
                String value = "";
                try {
                    key = URLEncoder.encode(entry.getKey(), encoding);
                    value = URLEncoder.encode((String) entry.getValue(), encoding);
                } catch (UnsupportedEncodingException e) {
                }
                String nvp = key + "=" + value;
                endpoint += nvp;
            } else if (entry.getValue() instanceof List) {
                List<String> toAdd = new ArrayList<String>();
                String key = "";
                try {
                    key = URLEncoder.encode(entry.getKey(), encoding);
                } catch (UnsupportedEncodingException e) {
                }
                for (Object value : (List) entry.getValue()) {
                    toAdd.add(key + "=" + (String) value);
                }
                endpoint += TextUtils.join("&", toAdd);
            }
        }

        return endpoint;
    }

    private WebResult handleHttpResponse(HttpResponse response) throws IOException, WebException {
        if (response != null) {
            int responseCode = response.getStatusLine().getStatusCode();
            String message = response.getStatusLine().getReasonPhrase();

            if (responseCode == HttpStatusCode.OK || responseCode == HttpStatusCode.CREATED ||
                    responseCode == HttpStatusCode.ACCEPTED) {
                HttpEntity entity = response.getEntity();

                if (entity != null) {
                    String result = EntityUtils.toString(entity, HTTP.UTF_8);
                    return new WebResult(responseCode, new JsonResult(result), response.getAllHeaders());
                }
            } else {
                WebException e;
                switch (responseCode) {
                    case HttpStatusCode.UNAUTHORIZED:
                        e = new AuthorizationRequiredException(responseCode, message);
                        break;
                    case HttpStatusCode.NOT_FOUND:
                        e = new NotFoundException(responseCode, message);
                        break;
                    case HttpStatusCode.METHOD_NOT_ALLOWED:
                        e = new MethodNotAllowedException(responseCode, message);
                        break;
                    case HttpStatusCode.BAD_REQUEST:
                        e = new WebClientException(responseCode, message);
                        break;
                    case HttpStatusCode.FORBIDDEN:
                        e = new WebClientException(responseCode, message);
                        break;
                    case HttpStatusCode.PROXY_AUTHENTICATION_REQUIRED:
                        e = new WebClientException(responseCode, message);
                        break;
                    case HttpStatusCode.REQUEST_TIMEOUT:
                        e = new WebClientException(responseCode, message);
                        break;
                    case HttpStatusCode.CONFLICT:
                    case HttpStatusCode.SERVER_ERROR:
                    case HttpStatusCode.NOT_IMPLEMENTED:
                    case HttpStatusCode.BAD_GATEWAY:
                    case HttpStatusCode.SERVICE_UNAVAILABLE:
                    case HttpStatusCode.GATEWAY_TIMEOUT:
                        e = new WebServerException(responseCode, message);
                        break;
                    default:
                        e = new WebException(responseCode, message);
                }
                throw e;
            }
        }

        return null;
    }

    protected class HttpStatusCode {
        private static final int UNHANDLED_EXCEPTION = -1;

        private static final int OK = 200;
        private static final int CREATED = 201;
        private static final int ACCEPTED = 202;

        private static final int BAD_REQUEST = 400;
        private static final int UNAUTHORIZED = 401;
        private static final int FORBIDDEN = 402;
        private static final int NOT_FOUND = 404;
        private static final int METHOD_NOT_ALLOWED = 405;
        private static final int PROXY_AUTHENTICATION_REQUIRED = 407;
        private static final int REQUEST_TIMEOUT = 408;
        private static final int CONFLICT = 409;

        private static final int SERVER_ERROR = 500;
        private static final int NOT_IMPLEMENTED = 501;
        private static final int BAD_GATEWAY = 502;
        private static final int SERVICE_UNAVAILABLE = 503;
        private static final int GATEWAY_TIMEOUT = 504;
    }
}
