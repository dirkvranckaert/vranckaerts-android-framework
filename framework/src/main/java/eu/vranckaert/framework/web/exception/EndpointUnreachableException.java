package eu.vranckaert.framework.web.exception;

/**
 * Date: 14/04/14
 * Time: 13:28
 *
 * @author Dirk Vranckaert
 */
public class EndpointUnreachableException extends RuntimeException {
    public EndpointUnreachableException(String endpoint) {
        super("The endpoint cannot be reached, you might be offline: " + endpoint);
    }
}
